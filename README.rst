Fotocolleccion
===============

Online photo manager
++++++++++++++++++++

**---- Work in progress ----**


.. image:: ./doc/images/collections_view.png

.. image:: ./doc/images/photo_view.png

Description
-----------
The photo manager allows you to organize photos into collections, arrange them in a specific order, tag them, and provide captions. The main page shows all the collections a user has uploaded as simple cards which can be clicked to see the contents, i.e., the actual photos. These photos are automatically arranged into a justified grid independent of their format and size. The advantage of the grid is that the photos don't have to be cropped to be displayed in a visibly appealing way. Another feature of the photo manager is that a tag cloud is generated from all the photos a user has previously tagged.


The admin area gives a user access to the adding, editing, deleting, tagging and reorganizing the collections and photos themselves. Multiple accounts can be created so users can only edit their own photos. Items can be public or private.

Preview
-------
There is an older version of this application online which serves as a preview for this redesigned version while it is in progress.
You can see the preview here: https://loxosceles.me/

Built with
----------
| **Frontend:** React
| **Backend:** Django
| Postgres, GraphQL, Docker, AWS, Material-UI 
