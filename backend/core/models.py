from django.db import models
from django.contrib.auth import get_user_model


class Album(models.Model):
    # title, description, title_image, slug, user_id
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    slug = models.SlugField(unique=True)
    cover_url = models.URLField()
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(get_user_model(), null=True,
                             on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Photo(models.Model):
    # image, :album_id, :caption, position, tag_list
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    src = models.URLField()
    thumbnail = models.URLField()
    thumbnail_width = models.IntegerField()
    thumbnail_height = models.IntegerField()
    caption = models.CharField(max_length=25)
    position = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.caption
