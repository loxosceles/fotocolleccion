#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest
from graphene.test import Client
from app.schema import schema
from collections import OrderedDict


class TestAlbumSnaps:
    @classmethod
    def setup_class(cls):
        cls.client = Client(schema)

    def test_get_albums(self, albums, snapshot):
        """TODO: Write the query."""

        res = self.client.execute(
            '''
            query {}
            ''')
        snapshot.assert_match(res)

    @classmethod
    def teardown_class(cls):
        pass
