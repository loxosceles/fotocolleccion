#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import boto3
from botocore.exceptions import ClientError
from botocore.config import Config
import requests
from pydash import camel_case
from decouple import config

boto3.set_stream_logger(name='botocore')


#  def create_presigned_post(object_name, fields=None, conditions=None, expiration=3600):
def create_presigned_post(object_name, expiration=3600):
    """Generate a presigned URL S3 POST request to upload a file.

    Args:
        bucket_name (str): name of the AWS bucket
        object_name (str): name of the object to be uploaded
        fields (dict): prefilled form fields
        conditions (list): conditions to include in the policy
        expiration (int): Time in seconds for the presigned URL to remain valid

    Returns:
        Dictionary with the following keys:
            url: URL to post to
            fields: Dictionary of form fields and values to submit with the POST

        Returns None if error.
    """

    fields = {"acl": "public-read"}
    conditions = [
        {"acl": "public-read"}
    ]

    s3_client = boto3.client(
        's3',
        config=Config(signature_version='s3v4', s3={'addressing_style': 'virtual'}),
        region_name=config('AWS_REGION'),
        aws_access_key_id=config('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=config('AWS_SECRET_ACCESS_KEY'),
    )

    try:
        response = s3_client.generate_presigned_post(config('AWS_BUCKET'),
                                                     object_name,
                                                     Fields=fields,
                                                     Conditions=conditions,
                                                     ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None

    response['fields'] = {camel_case(k): v for (k, v) in response['fields'].items()}
    print(response['fields'])
    return response

def upload(response, object_name):
    with open(object_name, 'rb') as f:
        files = {'file': (object_name, f)}
        http_response = requests.post(
            response['url'], data=response['fields'], files=files)

    print(f'File upload HTTP status code: {http_response.status_code}')
