#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import graphene
from graphene_django import DjangoObjectType
from graphql import GraphQLError
#  from django.db.models import Q

from .interfaces import create_presigned_post

class Fields(graphene.ObjectType):
    key = graphene.String()
    policy = graphene.String()
    xAmzAlgorithm = graphene.String()
    xAmzCredential = graphene.String()
    xAmzDate = graphene.String()
    xAmzSignature = graphene.String()
    acl = graphene.String()


class PreSignedURL(graphene.ObjectType):
    url = graphene.String()
    fields = graphene.Field(Fields)


class Query(graphene.ObjectType):
    presigned_url = graphene.Field(PreSignedURL, file_name=graphene.String(),
                                   required=True)

    def resolve_presigned_url(self, info, file_name):
        response = create_presigned_post(file_name)
        return response
