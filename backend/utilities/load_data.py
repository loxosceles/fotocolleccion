#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import walk, listdir, path, sep
import json
import uuid
from PIL import Image


BUCKET = 'https://photocollections-dev.s3.eu-central-1.amazonaws.com'
#  TARGET_DIR = '/mnt/DATA/@loxosceles/test_data'
TARGET_DIR = '/app/test_data'

directories = [_dir for _dir in listdir(TARGET_DIR) if path.isdir(_dir)]

print(f'directories: {directories}')

res = []
depth = 1

def get_dimensions(file_path):
    img = Image.open(file_path)
    return int(img.width), int(img.height)


u = [{
    "model": "auth.user",
    "pk": 1,
    "fields": {
        "password": "pbkdf2_sha256$150000$auL9gQ38QAmc$1DyaPIGlThzGluJzj2d+0RONu4TLsg96YeDGV050+8s=",
        "last_login": None,
        "is_superuser": False,
        "username": "testuser",
        "first_name": "",
        "last_name": "",
        "email": "testuser@example.com",
        "is_staff": True,
        "is_active": True,
        "date_joined": "2020-06-24T14:18:37.119Z",
        "groups": [],
        "user_permissions": []
    }
},
    {
        "model": "auth.user",
        "pk": 2,
        "fields": {
            "password": "pbkdf2_sha256$150000$auL9gQ38QAmc$1DyaPIGlThzGluJzj2d+0RONu4TLsg96YeDGV050+8s=",
            "last_login": None,
            "is_superuser": False,
            "username": "user",
            "first_name": "",
            "last_name": "",
            "email": "user@example.com",
            "is_staff": True,
            "is_active": True,
            "date_joined": "2020-06-24T14:18:37.119Z",
            "groups": [],
            "user_permissions": []
        }
}]

res.extend(u)

for idx, dir_path in enumerate(directories):
    a = {}
    a['model'] = 'core.album'
    a['pk'] = idx
    a['fields'] = {}
    a['fields']['title'] = dir_path
    a['fields']['description'] = ''
    a['fields']['slug'] = dir_path.lower()
    a['fields'][
        'cover_url'] = f"{BUCKET}/{dir_path.lower()}_cover.jpg"
    a['fields']['created_at'] = "2020-06-24T15:43:29.177Z"
    a['fields']['user'] = 1
    res.append(a)

for idx, dir_path in enumerate(directories):
    for root, dirs, files in walk(dir_path):
        if root[len(dir_path):].count(sep) < depth:
            pos = 0
            for file_idx, file in enumerate(files):
                img_width, img_height = get_dimensions(path.join(root, file))
                d = {}
                d['model'] = 'core.photo'
                d['pk'] = int(str(uuid.uuid4().int)[:4])
                d['fields'] = {}
                d['fields']['album'] = idx
                d['fields']['src'] = f"{BUCKET}/{dir_path}/20/{file}"
                d['fields']['thumbnail'] = f"{BUCKET}/{dir_path}/5/{file}"
                d['fields']['thumbnail_width'] = img_width
                d['fields']['thumbnail_height'] = img_height
                d['fields']['caption'] = f'caption {pos}'
                d['fields']['position'] = pos
                d['fields']['created_at'] = "2020-06-24T15:43:29.177Z"
                pos += 1
                res.append(d)

print(res)

with open('./fixtues/out_1.json', 'w') as outfile:
    json.dump(res, outfile)
