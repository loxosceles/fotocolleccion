#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import graphene
from graphene_django import DjangoObjectType
from graphql import GraphQLError
from django.db.models import Q

from core.models import Album


class AlbumType(DjangoObjectType):
    class Meta:
        model = Album


class Query(graphene.ObjectType):

    albums = graphene.List(AlbumType, search=graphene.String())
    album = graphene.Field(AlbumType, id=graphene.Int(), required=True)

    def resolve_albums(self, info, search=None):
        user = info.context.user or None

        #  if search:
        #      filtered = (
        #          Q(title__icontains=search) |
        #          Q(description__icontains=search) |
        #          Q(user__username=search)
        #      )
        #      return Album.objects.filter(filtered)

        return Album.objects.filter(user=user)

    def resolve_album(self, info, id=id):
        return Album.objects.get(pk=id)


class CreateAlbum(graphene.Mutation):
    album = graphene.Field(AlbumType)

    class Arguments:
        title = graphene.String()
        description = graphene.String()
        slug = graphene.String()
        cover_url = graphene.String()

    def mutate(self, info, **kwargs):
        #  def mutate(self, info, title, description, slug):
        user = info.context.user or None

        if user.is_anonymous:
            raise GraphQLError("Log in to add an album!")

        album = Album(**kwargs)
        #  album = Album(user=user, title=title, description=description,
        #                slug=slug)
        #  album = Album(title=title, description=description,
        #                slug=slug)
        #  album = Album(title="bla", description="blub", slug="slug")
        album.save()
        return CreateAlbum(album=album)


class UpdateAlbum(graphene.Mutation):
    album = graphene.Field(AlbumType)

    class Arguments:
        album_id = graphene.Int(required=True)
        title = graphene.String()
        description = graphene.String()
        slug = graphene.String()

    def mutate(self, info, album_id, title, slug, description=None):
        user = info. context.user
        album = Album.objects.get(id=album_id)

        if album.user != user:
            raise GraphQLError("Not permitted to update this album")

        album.title = title
        if description is not None:
            album.desription = description
        album.slug = slug

        album.save()
        return UpdateAlbum(album=album)


class DeleteAlbum(graphene.Mutation):
    album_id = graphene.Int()

    class Arguments:
        album_id = graphene.Int(required=True)

    def mutate(self, info, album_id):
        user = info.context.user
        album = Album.objects.get(id=album_id)

        if album.user != user:
            raise GraphQLError("Not permitted to delete this album")

        album.delete()
        return DeleteAlbum(album_id=album_id)


class Mutation(graphene.ObjectType):
    create_album = CreateAlbum.Field()
    update_album = UpdateAlbum.Field()
    delete_album = DeleteAlbum.Field()
