import pytest
from graphene.test import Client
from app.schema import schema
from collections import OrderedDict


class TestAlbumSnaps:
    @classmethod
    def setup_class(cls):
        cls.client = Client(schema)

    def test_get_albums(self, albums, snapshot):
        res = self.client.execute(
            '''
            query {
              albums {
                id
                title
                description
                slug
                user {
                  id
                  username
                  email
                }
              }
            }
            ''')
        snapshot.assert_match(res)

    def test_get_user(self, snapshot, user):
        res = self.client.execute(
            '''
            query {
              user(id:1) {
                id
                username
                email
              }
            }
            ''')
        snapshot.assert_match(res)

    def test_create_user(self, snapshot, user):
        res = self.client.execute(
            '''
            query {
              user(id:2) {
                id
                username
                email
              }
            }
            ''')
        assert res["errors"][0]["message"] == "User matching query does not exist."
        self.client.execute(
            '''
            mutation {
                createUser(username:"test_user",
                           email:"test_user@example.com",
                           password:"password") {
                    user {
                        id
                        username
                        email
                    }
                }
            }
            ''')
        res = self.client.execute(
            '''
            query {
              user(id:2) {
                id
                username
                email
              }
            }
            ''')
        snapshot.assert_match(res)

    def test_create_album(self, snapshot, user):
        query = '''
            query {
              albums {
                title
              }
            }
            '''
        res = self.client.execute(query)
        assert not OrderedDict({"title": "test album"}) in res["data"]["albums"]
        res = self.client.execute(
            '''
            mutation {
              createAlbum(title:"test album",
                          description:"test description",
                          slug:"test-slug") {
                album {
                  id
                  title
                  description
                  slug
                  createdAt
                }
              }
            }
            ''', context=pytest.helpers.user_context(user))
        res = self.client.execute(query)
        assert OrderedDict({"title": "test album"}) in res["data"]["albums"]
        snapshot.assert_match(res)

    def test_update_album(self, snapshot, albums, user):
        query = '''
            query {
              albums {
                id
                title
                description
              }
            }
            '''
        res = self.client.execute(query)
        assert not OrderedDict({"title": "Laos"}) in res["data"]["albums"]
        self.client.execute(
            '''
            mutation {
              updateAlbum(albumId:1,
              title:"Laos",
              slug:"laos") {
                album {
                  id
                  title
                }
              }
            }
            ''', context=pytest.helpers.user_context(user))
        res = self.client.execute(query)
        #  assert OrderedDict({"title": "Thailand"}) in res["data"]["albums"]
        #  assert not OrderedDict({"title": "Laos"}) in res["data"]["albums"]
        snapshot.assert_match(res)

    def test_update_album_wrong_user(self, snapshot, other_user, albums):
        query = '''
            query {
              albums {
                id
                title
                description
              }
            }
            '''
        res = self.client.execute(query)
        assert not OrderedDict({"title": "Laos"}) in res["data"]["albums"]
        res = self.client.execute(
            '''
            mutation {
              updateAlbum(albumId:1,
              title:"Laos",
              slug:"laos") {
                album {
                  id
                  title
                  description
                }
              }
            }
            ''', context=pytest.helpers.user_context(other_user))
        res = self.client.execute(query)
        assert not OrderedDict({"title": "Laos"}) in res["data"]["albums"]
        snapshot.assert_match(res)

    def test_delete_album(self, snapshot, albums, user):
        query = '''
            query {
              albums {
                id
              }
            }
            '''
        res = self.client.execute(query)
        assert OrderedDict({"id": "1"}) in res["data"]["albums"]
        res = self.client.execute(
            '''
            mutation {
              deleteAlbum(albumId:1) {
                albumId
              }
            }
            ''', context=pytest.helpers.user_context(user))
        res = self.client.execute(query)
        assert OrderedDict({"id": "1"}) not in res["data"]["albums"]
        snapshot.assert_match(res)

    def test_delete_album_wrong_user(self, snapshot, other_user, albums):
        query = '''
            query {
              albums {
                id
              }
            }
            '''
        res = self.client.execute(query)
        assert OrderedDict({"id": "1"}) in res["data"]["albums"]
        res = self.client.execute(
            '''
            mutation {
              deleteAlbum(albumId:1) {
                albumId
              }
            }
            ''', context=pytest.helpers.user_context(other_user))
        res = self.client.execute(query)
        assert OrderedDict({"id": "1"}) in res["data"]["albums"]
        snapshot.assert_match(res)

    @classmethod
    def teardown_class(cls):
        pass
