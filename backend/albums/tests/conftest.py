import pytest
from pytest_factoryboy import register
from seed.factories import (UserFactory,
                            AlbumFactory)

register(UserFactory)
register(AlbumFactory)


@pytest.fixture
def albums(album_factory, user):
    return album_factory.create_batch(
        size=2,
        #  title=Sequence(lambda n: f'Album {n}'),
        #  description=Sequence(lambda n: f'Description {n}'),
        #  slug=Sequence(lambda n: f'slug-{n}'),
        user=user)
