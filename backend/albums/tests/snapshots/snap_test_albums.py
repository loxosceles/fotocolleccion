# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['TestAlbumSnaps.test_get_user 1'] = {
    'data': {
        'user': {
            'email': 'lemmykilmister@example.com',
            'id': '1',
            'username': 'lemmykilmister'
        }
    }
}

snapshots['TestAlbumSnaps.test_create_user 1'] = {
    'data': {
        'user': {
            'email': 'test_user@example.com',
            'id': '2',
            'username': 'test_user'
        }
    }
}

snapshots['TestAlbumSnaps.test_create_album 1'] = {
    'data': {
        'albums': [
            {
                'title': 'test album'
            }
        ]
    }
}

snapshots['TestAlbumSnaps.test_get_albums 1'] = {
    'data': {
        'albums': [
            {
                'description': 'Each cause bill scientist nation.',
                'id': '1',
                'slug': 'need',
                'title': 'Police',
                'user': {
                    'email': 'lemmykilmister@example.com',
                    'id': '1',
                    'username': 'lemmykilmister'
                }
            },
            {
                'description': 'Officer relate animal direction eye bag. Term herself law street class. Decide environment view possible participant commercial. Clear here writer policy news.',
                'id': '2',
                'slug': 'gun',
                'title': 'Stop',
                'user': {
                    'email': 'lemmykilmister@example.com',
                    'id': '1',
                    'username': 'lemmykilmister'
                }
            }
        ]
    }
}

snapshots['TestAlbumSnaps.test_update_album 1'] = {
    'data': {
        'albums': [
            {
                'description': '''Management sense technology check civil quite others. High you more wife team activity.
Seem shoulder future fall citizen about reveal. Will seven medical blood personal.''',
                'id': '1',
                'title': 'Laos'
            },
            {
                'description': 'Campaign little near enter. Institution deep much role cut find yet practice. Tonight later easy ask again network.',
                'id': '2',
                'title': 'Within'
            }
        ]
    }
}

snapshots['TestAlbumSnaps.test_delete_album 1'] = {
    'data': {
        'albums': [
            {
                'id': '2'
            }
        ]
    }
}

snapshots['TestAlbumSnaps.test_update_album_wrong_user 1'] = {
    'data': {
        'albums': [
            {
                'description': '''Couple large instead everything economic. Kitchen technology nearly anything.
Why unit support. Image loss ten total. Her world enter six.''',
                'id': '1',
                'title': 'Technology'
            },
            {
                'description': 'Surface attention attack technology. Build three east organization people information. North first end prove fire enter capital population.',
                'id': '2',
                'title': 'Church'
            }
        ]
    }
}

snapshots['TestAlbumSnaps.test_delete_album_wrong_user 1'] = {
    'data': {
        'albums': [
            {
                'id': '1'
            },
            {
                'id': '2'
            }
        ]
    }
}
