from django.utils import timezone as tz
from random import randint
import factory
from faker import Faker
import factory.random
from django.template.defaultfilters import slugify

from core.models import (Album, Photo)

faker = Faker()
Faker.seed(42)


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = 'auth.User'
        django_get_or_create = ('username',)

    first_name = 'Lemmy'
    last_name = 'Kilmister'
    username = factory.LazyAttribute(lambda o: slugify(
        o.first_name + '.' + o.last_name))
    email = factory.LazyAttribute(lambda o: o.username + "@example.com")

    @factory.lazy_attribute
    def date_joined(self):
        return tz.now() - tz.timedelta(days=randint(5, 50))

    last_login = factory.LazyAttribute(
        lambda o: o.date_joined + tz.timedelta(days=4))


class AlbumFactory(factory.DjangoModelFactory):
    class Meta:
        model = Album

    title = factory.Sequence(lambda _: faker.word().capitalize())
    description = factory.Sequence(lambda _: faker.text())
    slug = factory.Sequence(lambda _: faker.word())
    @factory.lazy_attribute
    def created_at(self):
        return factory.LazyFunction(
            tz.now() - tz.timedelta(days=randint(5, 50)))
    user = factory.SubFactory(UserFactory)


class PhotoFactory(factory.DjangoModelFactory):
    class Meta:
        model = Photo

    album = factory.SubFactory(AlbumFactory)
    caption = factory.Sequence(lambda _: faker.word().capitalize())
    position = factory.Sequence(lambda _: _)
