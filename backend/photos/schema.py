import graphene
from graphene_django import DjangoObjectType
from graphql import GraphQLError
#  from django.db.models import Q

from core.models import Photo, Album


class PhotoType(DjangoObjectType):
    class Meta:
        model = Photo


class Query(graphene.ObjectType):
    photos = graphene.List(PhotoType, albumId=graphene.Int(), required=True)

    def resolve_photos(self, info, albumId):
        user = info.context.user or None
        album = Album.objects.get(pk=albumId)

        if album.user == user:
            return Photo.objects.filter(album=album)

        return Photo.objects.none()


class CreatePhoto(graphene.Mutation):
    photo = graphene.Field(PhotoType)

    class Arguments:
        album_id = graphene.Int(required=True)
        src = graphene.String(required=True)
        thumbnail = graphene.String(required=True)
        thumbnail_width = graphene.Int(required=True)
        thumbnail_height = graphene.Int(required=True)

    def mutate(self, info, album_id, src, thumbnail, thumbnail_height,
               thumbnail_width, *args):
        user = info.context.user or None
        album = Album.objects.get(id=album_id) or None

        if user.is_anonymous:
            raise GraphQLError("Log in to add an photo!")

        if not album:
            raise GraphQLError("Album does not exist!")

        if album.user != user:
            raise GraphQLError("Not permitted to add photo to album!")

        position = len(album.photo_set.all()) + 1
        caption = f'Caption {position}'

        photo = Photo(album=album, src=src, thumbnail=thumbnail,
                      thumbnail_width=thumbnail_width,
                      thumbnail_height=thumbnail_height,
                      position=position,
                      caption=caption,
                      *args)

        photo.save()

        return CreatePhoto(photo=photo)


class UpdatePhoto(graphene.Mutation):
    photo = graphene.Field(PhotoType)

    class Arguments:
        photo_id = graphene.Int(required=True)
        caption = graphene.String()
        postion = graphene.Int()

    def mutate(self, info, photo_id, caption=None, position=None):
        user = info. context.user
        photo = Photo.objects.get(id=photo_id)
        album = photo.album

        if album.user != user:
            raise GraphQLError("Not permitted to update this photo")

        if caption:
            photo.caption = caption
        if position:
            photo.postion = position

        photo.save()
        return UpdatePhoto(photo=photo)


class DeletePhoto(graphene.Mutation):
    photo_id = graphene.Int()

    class Arguments:
        photo_id = graphene.Int(required=True)

    def mutate(self, info, photo_id):
        user = info.context.user
        photo = Photo.objects.get(id=photo_id)
        album = photo.album

        if album.user != user:
            raise GraphQLError("Not permitted to delete this photo")

        photo.delete()
        return DeletePhoto(photo_id=photo_id)


class Mutation(graphene.ObjectType):
    create_photo = CreatePhoto.Field()
    update_photo = UpdatePhoto.Field()
    delete_photo = DeletePhoto.Field()
