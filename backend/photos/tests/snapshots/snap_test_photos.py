# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['TestPhotoSnaps.test_get_photos 1'] = {
    'data': {
        'photos': [
            {
                'album': {
                    'id': '1',
                    'title': 'Wear'
                },
                'caption': 'Offer',
                'id': '1'
            },
            {
                'album': {
                    'id': '1',
                    'title': 'Wear'
                },
                'caption': 'Local',
                'id': '2'
            }
        ]
    }
}

snapshots['TestPhotoSnaps.test_photo_belongs_to_album 1'] = {
    'data': {
        'photos': [
            {
                'album': {
                    'id': '1',
                    'title': 'Wall'
                },
                'id': '1',
                'position': 2
            },
            {
                'album': {
                    'id': '1',
                    'title': 'Wall'
                },
                'id': '2',
                'position': 3
            }
        ]
    }
}

snapshots['TestPhotoSnaps.test_create_photo 1'] = {
    'data': {
        'photos': [
            {
                'caption': 'test photo'
            }
        ]
    }
}

snapshots['TestPhotoSnaps.test_create_photo_wrong_user 1'] = {
    'data': {
        'photos': [
        ]
    }
}

snapshots['TestPhotoSnaps.test_update_photo_caption 1'] = {
    'data': {
        'photos': [
            {
                'caption': 'Updated caption'
            }
        ]
    }
}

snapshots['TestPhotoSnaps.test_update_photo_caption_wrong_user 1'] = {
    'data': {
        'photos': [
            {
                'caption': 'Just',
                'id': '1'
            }
        ]
    }
}

snapshots['TestPhotoSnaps.test_delete_photo 1'] = {
    'data': {
        'photos': [
        ]
    }
}

snapshots['TestPhotoSnaps.test_delete_photo_wrong_user 1'] = {
    'data': {
        'photos': [
            {
                'id': '1'
            }
        ]
    }
}
