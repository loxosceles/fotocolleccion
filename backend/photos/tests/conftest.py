import pytest
from pytest_factoryboy import register
from seed.factories import (UserFactory,
                            AlbumFactory,
                            PhotoFactory)

register(UserFactory)
register(AlbumFactory)
register(PhotoFactory)


@pytest.fixture
def album(album_factory, user_factory):
    user = user_factory()
    album = album_factory(user=user)
    return album


@pytest.fixture
def photos(photo_factory, album):
    return photo_factory.create_batch(
        size=2,
        album=album)
