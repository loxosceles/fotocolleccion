import pytest
from graphene.test import Client
from app.schema import schema
from collections import OrderedDict


class TestPhotoSnaps:
    @classmethod
    def setup_class(cls):
        cls.client = Client(schema)

    def test_get_photos(self, photos, snapshot):
        res = self.client.execute(
            '''
            query {
              photos {
                id
                caption
                album {
                  id
                  title
                }
              }
            }
            ''')
        snapshot.assert_match(res)

    def test_photo_belongs_to_album(self, photos, snapshot):
        res = self.client.execute(
            '''
            query {
              photos {
                id
                position
                album {
                 id
                  title
                }
              }
            }
            ''')
        snapshot.assert_match(res)

    def test_create_photo(self, snapshot, user, album):
        query = '''
            query {
              photos {
                caption
              }
            }
            '''
        res = self.client.execute(query)
        assert not OrderedDict({"caption": "test photo"}) in res["data"]["photos"]
        res = self.client.execute(
            '''
            mutation {
              createPhoto(albumId: 1, caption:"test photo",
                          position:1) {
                photo {
                  id
                  caption
                  position
                }
              }
            }
            ''', context=pytest.helpers.user_context(user))
        res = self.client.execute(query)
        assert OrderedDict({"caption": "test photo"}) in res["data"]["photos"]
        snapshot.assert_match(res)

    def test_create_photo_wrong_user(self, snapshot, other_user, album):
        query = '''
            query {
              photos {
                caption
              }
            }
            '''
        res = self.client.execute(query)
        assert not OrderedDict({"caption": "test photo"}) in res["data"]["photos"]
        res = self.client.execute(
            '''
            mutation {
              createPhoto(albumId: 1, caption:"test photo",
                          position:1) {
                photo {
                  id
                  caption
                  position
                }
              }
            }
            ''', context=pytest.helpers.user_context(other_user))
        res = self.client.execute(query)
        assert not OrderedDict({"caption": "test photo"}) in res["data"]["photos"]
        snapshot.assert_match(res)

    def test_update_photo_caption(self, snapshot, user, photo):
        query = '''
                query {
                  photos {
                    caption
                  }
                }
            '''
        res = self.client.execute(query)
        assert not OrderedDict({"caption": "test caption"}) in res["data"]["photos"]
        res = self.client.execute(
            '''
            mutation {
              updatePhoto(photoId:1, caption:"Updated caption"){
                photo {
                  id
                  caption
                }
              }
            }
            ''', context=pytest.helpers.user_context(user))
        res = self.client.execute(query)
        assert not OrderedDict({"caption": "test photo"}) in res["data"]["photos"]
        assert OrderedDict({"caption": "Updated caption"}) in res["data"]["photos"]
        snapshot.assert_match(res)

    def test_update_photo_caption_wrong_user(self, snapshot, other_user, photo):
        query = '''
                query {
                  photos {
                    id
                    caption
                  }
                }
            '''
        res = self.client.execute(query)
        assert not OrderedDict({"caption": "test caption"}) in res["data"]["photos"]
        res = self.client.execute(
            '''
            mutation {
              updatePhoto(photoId:1, caption:"Updated caption"){
                photo {
                  id
                  caption
                }
              }
            }
            ''', context=pytest.helpers.user_context(other_user))
        #  import pdb; pdb.set_trace()
        res = self.client.execute(query)
        assert not OrderedDict({"caption": "test photo"}) in res["data"]["photos"]
        snapshot.assert_match(res)

    def test_delete_photo(self, snapshot, user, photo):
        query = '''
                query {
                  photos {
                    id
                  }
                }
            '''
        res = self.client.execute(query)
        assert OrderedDict({"id": "1"}) in res["data"]["photos"]
        res = self.client.execute(
            '''
            mutation {
              deletePhoto(photoId:1){
                photoId
              }
            }
            ''', context=pytest.helpers.user_context(user))
        res = self.client.execute(query)
        assert not OrderedDict({"id": "1"}) in res["data"]["photos"]
        snapshot.assert_match(res)

    def test_delete_photo_wrong_user(self, snapshot, other_user, photo):
        query = '''
                query {
                  photos {
                    id
                  }
                }
            '''
        res = self.client.execute(query)
        assert OrderedDict({"id": "1"}) in res["data"]["photos"]
        res = self.client.execute(
            '''
            mutation {
              deletePhoto(photoId:1){
                photoId
              }
            }
            ''', context=pytest.helpers.user_context(other_user))
        res = self.client.execute(query)
        assert OrderedDict({"id": "1"}) in res["data"]["photos"]
        snapshot.assert_match(res)
