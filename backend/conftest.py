import pytest
pytest_plugins = ['helpers_namespace']


@pytest.helpers.register
def user_context(user):
    class Context:
        pass

    c = Context()
    setattr(c, "user", user)
    return c


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass


@pytest.fixture
def user(user_factory):
    return user_factory()


@pytest.fixture
def other_user(user_factory):
    return user_factory(first_name="Tom", last_name="Waits")
