import graphene
import users.schema
import albums.schema
import photos.schema
import upload.schema
import graphql_jwt


class Query(users.schema.Query,
            albums.schema.Query,
            photos.schema.Query,
            upload.schema.Query,
            graphene.ObjectType):
    pass


class Mutation(users.schema.Mutation,
               albums.schema.Mutation,
               photos.schema.Mutation,
               graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
