import React from 'react';
import ReactDOM from 'react-dom';
import {
  useQuery,
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
  createHttpLink,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

import { IS_LOGGED_IN } from './apollo/auth';
import { typeDefs } from './resolvers';
import App from './App';
import Login from './components/Auth/Login';

const httpLink = createHttpLink({
  uri: 'http://localhost:8000/graphql/',
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('authToken');
  return {
    headers: {
      ...headers,
      authorization: token ? `JWT ${token}` : '',
    },
  };
});

const iMCache = new InMemoryCache({
  // addTypename: false,
});

const client = new ApolloClient({
  cache: iMCache,
  link: authLink.concat(httpLink),
  typeDefs,
});

const IsLoggedIn = () => {
  const { data } = useQuery(IS_LOGGED_IN);
  return data && data.isLoggedIn ? <App /> : <Login />;
};

client.writeQuery({
  query: IS_LOGGED_IN,
  data: {
    isLoggedIn: !!localStorage.getItem('authToken'),
  },
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <IsLoggedIn />
  </ApolloProvider>,
  document.getElementById('root')
);
