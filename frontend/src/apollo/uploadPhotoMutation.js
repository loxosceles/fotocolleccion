import { gql } from '@apollo/client';

export const UPLOAD_PHOTO_MUTATION = gql`
  mutation UploadPhoto(
    $albumId: Int!
    $src: String!
    $thumbnail: String!
    $thumbnailHeight: Int!
    $thumbnailWidth: Int!
  ) {
    createPhoto(
      albumId: $albumId
      src: $src
      thumbnail: $thumbnail
      thumbnailHeight: $thumbnailHeight
      thumbnailWidth: $thumbnailWidth
    ) {
      photo {
        id
        src
        thumbnail
        thumbnailWidth
        thumbnailHeight
        caption
      }
    }
  }
`;
