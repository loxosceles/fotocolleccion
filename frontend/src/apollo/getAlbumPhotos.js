import { gql } from '@apollo/client';

export const GET_ALBUM_PHOTOS = gql`
  query($albumId: Int!) {
    photos(albumId: $albumId) {
      id
      caption
      src
      thumbnail
      thumbnailHeight
      thumbnailWidth
    }
  }
`;

// export const GET_ALBUM_PHOTOS = gql`
//   query($albumId: Int!) {
//     album(id: $albumId) {
//       title
//       photoSet {
//         id
//         caption
//         src
//         thumbnail
//         thumbnailHeight
//         thumbnailWidth
//       }
//     }
//   }
// `;
