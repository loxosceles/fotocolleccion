import { gql } from '@apollo/client';

export const GET_ALBUMS = gql`
  {
    albums {
      id
      title
      description
      slug
      coverUrl
    }
  }
`;

// export const GET_ALBUMS = gql`
//   query($userId: Int!) {
//     user(id: $userId) {
//       albumSet {
//         id
//         title
//         description
//         slug
//         coverUrl
//       }
//     }
//   }
// `;
