import { gql } from '@apollo/client';

export const GET_PRESIGNED_URL = gql`
  query($fileName: String!) {
    presignedUrl(fileName: $fileName) {
      url
      fields {
        key
        policy
        xAmzDate
        xAmzAlgorithm
        xAmzSignature
        xAmzCredential
        acl
      }
    }
  }
`;
