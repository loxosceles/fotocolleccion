import { gql } from '@apollo/client';

export const GET_PROFILE = gql`
  query($id: Int!) {
    user(id: $id) {
      username
      email
      dateJoined
      albumSet {
        id
        photoSet {
          id
        }
      }
    }
  }
`;
