import { useCallback } from 'react';
import { useApolloClient } from '@apollo/client';

export default (query) => {
  const client = useApolloClient();
  return useCallback(
    (variables) =>
      client.query({
        query: query,
        variables: variables,
      }),
    [client]
  );
};
