import { useState } from 'react';

export default (initialValue) => {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue([]),
    bind: {
      onChange: (event) => {
        setValue(Array.from(event.target.files));
      },
    },
  };
};
