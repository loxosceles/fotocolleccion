import React, { useState } from 'react';
import { useQuery, gql } from '@apollo/client';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Error from './components/Shared/Error';
import Loading from './components/Shared/Loading';
import Header from './components/Shared/Header';
import Collections from './pages/Collections';
import Profile from './pages/Profile';
import RandomGallery from './pages/RandomGallery';
import { UserContext } from './contexts/UserContext';
import PhotoBoardSelector from './components/PhotoBoard/PhotoBoardSelector';

const App = () => {
  const [user, setUser] = useState(UserContext);

  let { loading, error, data } = useQuery(GET_ME_QUERY, {
    onCompleted: () => {
      setUser(data.me);
    },
  });

  if (loading) return <Loading />;
  if (error) return <Error error={error} />;

  return (
    <BrowserRouter>
      <UserContext.Provider value={{ user, setUser }}>
        <Header />
        <Switch>
          <Route
            exact
            user={user}
            path={`/profile/:${user.id}`}
            render={(routeProps) => <Profile {...routeProps} />}
          />
          <Route
            exact
            path="/collections"
            render={(routeProps) => <Collections {...routeProps} />}
          />
          <Route
            exact
            path="/collections/:collectionName"
            render={(routeProps) => <PhotoBoardSelector {...routeProps} />}
          />
          <Route
            exact
            path="/"
            render={(routeProps) => <RandomGallery {...routeProps} />}
          />
        </Switch>
      </UserContext.Provider>
    </BrowserRouter>
  );
};

export const GET_ME_QUERY = gql`
  {
    me {
      id
      username
      email
    }
  }
`;

export default App;
