import React from 'react';
import { useMutation } from '@apollo/client';
import axios from 'axios';
import kebabcaseKeys from 'kebabcase-keys';
import omitDeep from 'omit-deep';
import cloneDeep from 'lodash/cloneDeep';

// import Uppy from '@uppy/core';
// import { Dashboard } from '@uppy/react';
// import AwsS3 from '@uppy/aws-s3';
// import '@uppy/core/dist/style.css';
// import '@uppy/dashboard/dist/style.css';

import Loading from '../Shared/Loading';
import Error from '../Shared/Error';
import useLazyQuery from '../../hooks/useLazyQuery';
import useFileInput from '../../hooks/useFileInput';

import { GET_PRESIGNED_URL } from '../../apollo/getPresignedURL';
import { UPLOAD_PHOTO_MUTATION } from '../../apollo/uploadPhotoMutation';
import { GET_ALBUM_PHOTOS } from '../../apollo/getAlbumPhotos';

const BUCKET = process.env.REACT_APP_BUCKET;
const THUMB_BUCKET = process.env.REACT_APP_THUMB_BUCKET;
const REDUCTION_PERC = '5';

const readUploadedFileAsDataURL = (inputFile) => {
  const fileReader = new FileReader();

  return new Promise((resolve, reject) => {
    fileReader.onerror = () => {
      fileReader.abort();
      reject(new DOMException('Problem parsing input file.'));
    };

    fileReader.onload = () => {
      resolve(fileReader.result);
    };
    fileReader.readAsDataURL(inputFile);
  });
};

const getImageDimensions = (dataURL) => {
  const image = new Image();

  return new Promise((resolve, reject) => {
    image.onerror = () => {
      reject(new DOMException('Image file could not be loaded.'));
    };

    image.onload = () => {
      resolve({ height: image.height, width: image.width });
    };
    image.src = dataURL;
  });
};

const UploadPhotoForm = ({ albumId, albumTitle }) => {
  const { value, bind, reset } = useFileInput([]);
  const getPresignedURL = useLazyQuery(GET_PRESIGNED_URL);
  const [createPhoto] = useMutation(UPLOAD_PHOTO_MUTATION);

  const uploadFile = async (presignObj, file) => {
    presignObj = kebabcaseKeys(presignObj, {
      deep: true,
    });

    const dataURL = await readUploadedFileAsDataURL(file);
    const { height, width } = await getImageDimensions(dataURL);

    const formData = new FormData();

    Object.keys(presignObj.fields).forEach((key) => {
      formData.append(key, presignObj.fields[key]);
    });

    formData.append('file', file);

    axios
      .post(presignObj.url, formData, {
        headers: {
          Accept: file.type,
          'Content-Type': 'multipart/form-data',
        },
      })
      .then(() => {
        const srcPath = `${BUCKET}${albumTitle}/${file.name}`;
        const thumbPath = `${THUMB_BUCKET}${albumTitle}/${REDUCTION_PERC}/${file.name}`;
        createPhoto({
          variables: {
            albumId,
            src: srcPath,
            thumbnail: thumbPath,
            thumbnailWidth: width,
            thumbnailHeight: height,
          },
          update: (cache, { data: { createPhoto } }) => {
            const data = cache.readQuery({
              query: GET_ALBUM_PHOTOS,
              variables: { albumId },
            });
            const newPhoto = { ...createPhoto };
            newPhoto.photo.thumbnail = srcPath;
            cache.writeQuery({
              query: GET_ALBUM_PHOTOS,
              variables: { albumId },
              data: {
                ...data,
                photos: [...data.photos, newPhoto.photo],
              },
            });
          },
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const handleSubmit = async (evt) => {
    evt.preventDefault();
    for (const file of value) {
      const { data, loading, error } = await getPresignedURL({
        fileName: `${albumTitle}/${file.name}`,
      });

      if (error) return <Error error={error} />;
      if (loading) return <Loading />;
      if (data) {
        const cleanData = omitDeep(cloneDeep(data), '__typename');
        uploadFile(cleanData.presignedUrl, file);
      }
    }
    reset();
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <input type="file" multiple {...bind} />
        <input type="submit" value="Submit" />
      </form>
    </div>
  );
};

export default UploadPhotoForm;
