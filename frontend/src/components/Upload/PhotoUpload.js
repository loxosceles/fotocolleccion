import React, { Component } from 'react';
// import { useQuery } from '@apollo/client';
import Uppy from '@uppy/core';
import { Dashboard } from '@uppy/react';
import AwsS3 from '@uppy/aws-s3';

import '@uppy/core/dist/style.css';
import '@uppy/dashboard/dist/style.css';

import { GET_PRESIGNED_URL } from '../queries';

// CHECK THIS OUT: https://github.com/transloadit/uppy/issues/460#issuecomment-443306287

class PhotoUpload extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showInlineDashboard: true,
      open: false,
    };

    this.uppy = new Uppy({
      id: 'upload',
      autoProceed: true,
      restrictions: {
        maxFileSize: 10000000,
        maxNumberOfFiles: 30,
        minNumberOfFiles: 1,
        allowedFileTypes: ['image/*'],
      },
      onBeforeFileAdded: (currentFile, files) => {
        console.log('Adding files in dashboard', currentFile);
      },
      onBeforeUpload: (files) => {
        console.log('Uploading files', files);
      },
      // debug: true,
    });

    // this.uppy.use(Dashboard, {
    //     id: 'Dashboard',
    //     trigger: '.UppyModalOpenerBtn',
    //     inline: true,
    //     target: '.DashboardContainer',
    //     replaceTargetContent: true,
    //     showProgressDetails: true,
    //     note: 'Images and video only, 2–3 files, up to 1 MB',
    //     height: 470,
    //     metaFields: [
    //         { id: 'name', name: 'Name', placeholder: 'file name' },
    //         {
    //             id: 'caption',
    //             name: 'Caption',
    //             placeholder: 'describe what the image is about',
    //         },
    //     ],
    //     browserBackButtonClose: true,
    // });
  }

  UNSAFE_componentWillMount() {
    this.uppy.use(AwsS3, {
      async getUploadParameters(file) {
        return fetch('http://localhost:5000/presign_url', {
          method: 'post',
          headers: {
            accept: 'application/json',
            'content-type': 'application/json',
          },
          body: JSON.stringify({
            filename: file.name,
            contentType: file.type,
            metadata: {
              name: file.meta['name'], // here we pass the 'name' variable to the back end, with 'file.meta['name']' referring to the 'name' from our metaFields id above
              caption: file.meta['caption'], // here we pass the 'caption' variable to the back end, with 'file.meta['caption']' referring to the 'caption' from our metaFields id above
            },
          }),
        })
          .then((response) => {
            // Parse the JSON response.
            return response.json();
          })
          .then((data) => {
            // Return an object in the correct shape.
            return {
              method: data.method,
              url: data.url,
              fields: data.fields,
              headers: data.headers,
            };
          });
      },
    });

    // this.handleModalClick = this.handleModalClick.bind(this);
  }

  componentWillUnmount() {
    this.uppy.close();
  }

  // handleModalClick() {
  //     this.setState({
  //         open: !this.state.open,
  //     });
  // }
  //
  render() {
    const { showInlineDashboard } = this.state;
    return (
      <div>
        <h1>PhotoCollections Upload</h1>
        <label>
          <input
            type="checkbox"
            checked={showInlineDashboard}
            onChange={(event) => {
              this.setState({
                showInlineDashboard: event.target.checked,
              });
            }}
          />
          Show Dashboard
        </label>
        {showInlineDashboard && (
          <Dashboard
            uppy={this.uppy}
            id="Dashboard"
            trigger=".UppyModalOpenerBtn"
            // target=".DashboardContainer"
            target="body"
            replaceTargetContent
            showProgressDetails
            proudlyDisplayPoweredByUppy={false}
            inline
            note="Images only, 20 files max., up to 1 MB"
            // plugins={['AwsS3']}
            metaFields={[
              {
                id: 'name',
                name: 'Name',
                placeholder: 'File name',
              },
            ]}
          />
        )}
      </div>
    );
  }
}

export default PhotoUpload;

// {
//     "method": "PUT",
//     "url": "https://uppy-test.s3.eu-west-2.amazonaws.com/uppy-php-example/?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAYE6YSW66VWLDD73H%2F20200629%2Feu-west-2%2Fs3%2Faws4_request&X-Amz-Date=20200629T221100Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Signature=99976beb18e41ee31c70d969b2396e36d8d502380ed61ae1d0911ef6a7d80180",
//     "fields": [],
//     "headers": {
//         "content-type": null
//     }
// }
