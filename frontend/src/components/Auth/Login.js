import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import Lock from '@material-ui/icons/Lock';
import {
  Typography,
  Avatar,
  FormControl,
  Paper,
  Input,
  InputLabel,
  Button,
} from '@material-ui/core';

import Error from '../Shared/Error';

import useStyles from '../../styles/loginStyles';

import { IS_LOGGED_IN, LOGIN_MUTATION } from '../../apollo/auth';

const Login = ({ setNewUser }) => {
  const classes = useStyles();

  const [username, setUsername] = useState('testuser');
  const [password, setPassword] = useState('password');

  const [tokenAuth, { loading, error, client }] = useMutation(LOGIN_MUTATION, {
    onCompleted({ tokenAuth }) {
      localStorage.setItem('authToken', tokenAuth.token);
      client.writeQuery({
        query: IS_LOGGED_IN,
        data: {
          isLoggedIn: true,
        },
      });
    },
  });

  const handleSubmit = async (event, tokenAuth) => {
    event.preventDefault();
    tokenAuth({ variables: { username, password } });
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <Lock />
        </Avatar>
        <Typography variant="h4">Login as Existing User</Typography>
        <form
          onSubmit={(event) => handleSubmit(event, tokenAuth)}
          className={classes.form}
        >
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="username">Username</InputLabel>
            <Input
              id="username"
              value={username}
              onChange={(event) => setUsername(event.target.value)}
            />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              id="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            disabled={loading || !username.trim() || !password.trim()}
            className={classes.submit}
          >
            {loading ? 'Logging in ...' : 'Login'}
          </Button>
          <Button
            onClick={() => setNewUser(true)}
            color="secondary"
            variant="outlined"
            fullWidth
          >
            Previous user? Log in here
          </Button>
          {/* Error Handling */}
          {error && <Error error={error} />}
        </form>
      </Paper>
    </div>
  );
};

export default Login;
