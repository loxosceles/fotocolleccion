import React, { useState } from 'react';
import { useApolloClient } from '@apollo/client';
import ExitToApp from '@material-ui/icons/ExitToApp';
import { Typography, Button } from '@material-ui/core';

import useStyles from '../../styles/signoutStyles';

import { IS_LOGGED_IN } from '../../apollo/auth';
import { UserContext } from '../../contexts/UserContext';

const Signout = () => {
  const client = useApolloClient();

  const [user, setUser] = useState(UserContext);
  const classes = useStyles();

  const handleSignout = () => {
    localStorage.clear();
    setUser(null);
    client.writeQuery({
      query: IS_LOGGED_IN,
      data: {
        isLoggedIn: false,
      },
    });
  };

  return (
    <Button onClick={() => handleSignout()}>
      <Typography
        variant="body1"
        className={classes.buttonText}
        color="secondary"
      >
        Signout
      </Typography>
      <ExitToApp className={classes.buttonIcon} color="secondary" />
    </Button>
  );
};

export default Signout;
