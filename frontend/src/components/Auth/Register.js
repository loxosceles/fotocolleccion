import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import Gavel from '@material-ui/icons/Gavel';
import VerifiedUserTwoTone from '@material-ui/icons/VerifiedUserTwoTone';
import {
  Typography,
  Avatar,
  FormControl,
  Paper,
  Input,
  InputLabel,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Slide,
} from '@material-ui/core';
import Error from '../Shared/Error';

import { REGISTER_MUTATION } from '../../apollo/registerMutation';
import useStyles from '../../styles/registerStyles';

export const Transition = React.forwardRef((props, ref) => (
  <Slide {...props} ref={ref} />
));

const Register = ({ setNewUser }) => {
  const classes = useStyles();
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [open, setOpen] = useState(false);

  const [createUser, { loading, error, data, client }] = useMutation(
    REGISTER_MUTATION,
    {
      variables: { username, email, password },
      onCompleted: (data) => {
        console.log({ data });
        setOpen(true);
      },
    }
  );

  const handleSubmit = (event, createUser) => {
    event.preventDefault();
    createUser();
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <Gavel />
        </Avatar>
        <Typography variant="h4">Register</Typography>
        <form
          onSubmit={(event) => handleSubmit(event, createUser)}
          className={classes.form}
        >
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="username">Username</InputLabel>
            <Input
              id="username"
              onChange={(event) => setUsername(event.target.value)}
            />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="email">Email</InputLabel>
            <Input
              id="email"
              type="email"
              onChange={(event) => setEmail(event.target.value)}
            />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              id="password"
              onChange={(event) => setPassword(event.target.value)}
            />
          </FormControl>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            disabled={
              loading || !username.trim() || !email.trim() || !password.trim()
            }
            className={classes.submit}
          >
            {loading ? 'Registering...' : 'Register'}
          </Button>
          <Button
            onClick={() => setNewUser(false)}
            color="primary"
            variant="outlined"
            fullWidth
          >
            Previous user? Log in here
          </Button>
          {/* Error Handling */}
          {error && <Error error={error} />}
        </form>
      </Paper>

      {/* Success Dialog */}
      <Dialog
        disableBackdropClick={true}
        open={open}
        TransitionComponent={Transition}
      >
        <DialogTitle>
          <VerifiedUserTwoTone className={classes.icon} />
          New Account
        </DialogTitle>
        <DialogContent>
          <DialogContentText>User successfully created!</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            color="primary"
            variant="contained"
            onClick={() => setNewUser(false)}
          >
            Login
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default Register;
