import React from 'react';

function PhotoCard({ photo }) {
  return (
    <div style={{ width: '50px', height: '50px' }}>
      <img src={photo.url} alt={photo.caption} />
      <p>photo.caption</p>
    </div>
  );
}

export default PhotoCard;
