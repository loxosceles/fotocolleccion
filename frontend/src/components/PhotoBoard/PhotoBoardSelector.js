import React from 'react';
import { useQuery } from '@apollo/client';

import PhotoBoard from '../../pages/PhotoBoard';
import Error404 from '../../pages/Error404';
import Loading from '../../components/Shared/Loading';
import Error from '../../components/Shared/Error';

import { GET_ALBUMS } from '../../apollo/getAlbums';

const PhotoBoardSelector = (props) => {
  let { called, loading, error, data } = useQuery(GET_ALBUMS);

  if (called && loading) return <Loading />;
  if (error) return <Error error={error} />;

  let collectionName = props.match.params.collectionName;
  let currentCollection = data.albums.find(
    (collection) =>
      collection.slug.toLowerCase() === collectionName.toLowerCase()
  );

  return currentCollection ? (
    <PhotoBoard {...props} collection={currentCollection} />
  ) : (
    <Error404 />
  );
};

export default PhotoBoardSelector;
