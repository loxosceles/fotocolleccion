import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import CameraAltSharpIcon from '@material-ui/icons/CameraAltSharp';
import FaceIcon from '@material-ui/icons/FaceTwoTone';
import { AppBar, Toolbar, Typography } from '@material-ui/core';

import { UserContext } from '../../contexts/UserContext';

import Signout from '../Auth/Signout';

import useStyles from '../../styles/headerStyles';

const Header = () => {
  const { user } = useContext(UserContext);
  const classes = useStyles();

  return (
    <AppBar position="static" className={classes.root}>
      <Toolbar>
        <Link to="/" className={classes.grow}>
          <CameraAltSharpIcon className={classes.logo} color="secondary" />
          <Typography variant="h4" color="secondary" noWrap>
            Photo Collections
          </Typography>
        </Link>
        {user && (
          <Link to={`/profile/${user.id}`} className={classes.grow}>
            <FaceIcon className={classes.faceIcon} />
            <Typography variant="h4" className={classes.username} noWrap>
              {user.username}
            </Typography>
          </Link>
        )}
        <Signout />
      </Toolbar>
    </AppBar>
  );
};

export default Header;
