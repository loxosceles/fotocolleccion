import React from 'react';
import { CircularProgress } from '@material-ui/core';

import useStyles from '../../styles/loadingStyles';

const Loading = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CircularProgress className={classes.progress} />
    </div>
  );
};

export default Loading;
