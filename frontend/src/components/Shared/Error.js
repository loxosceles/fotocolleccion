import React, { useState } from 'react';
import { Button, Snackbar } from '@material-ui/core';

import useStyles from '../../styles/errorStyles';

const Error = ({ error }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  return (
    <Snackbar
      open={open}
      className={classes.snackbar}
      message={error.message}
      action={
        <Button onClick={() => setOpen(false)} color="secondary" size="small">
          X
        </Button>
      }
    />
  );
};

export default Error;
