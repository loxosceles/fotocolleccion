import React from 'react';
import { Link } from 'react-router-dom';

function AlbumTile({ albums }) {
  return (
    <div>
      {albums.map((album) => (
        <div key={album.id}>
          <h4>{album.title}</h4>
          {/* <img src={album.coverUrl} alt={album.title} /> */}
          <div>{album.description}</div>
          <Link to={`/collections/${album.slug}`}>Gallery</Link>
        </div>
      ))}
    </div>
  );
}

export default AlbumTile;
