import React from 'react';
import { Link } from 'react-router-dom';
import Masonry from 'react-masonry-component';

const masonryOptions = {
  transitionDuration: 0,
};

const style = {
  backgroundColor: 'white',
  display: 'flex',
};

const AlbumList = ({ albums }) => {
  const handleClick = (evt) => {};

  const childElements = albums.map(function ({ id, title, coverUrl, slug }) {
    return (
      <li
        key={id}
        style={{
          textDecoration: 'none',
          listStyleType: 'none',
          alignItems: 'space-between',
          display: 'flex',
        }}
        className="image-element-class"
      >
        <Link to={`/collections/${slug}`}>
          <img
            style={{ width: '300px', margin: 'auto' }}
            src={coverUrl}
            alt={title}
          />
        </Link>
      </li>
    );
  });

  return (
    <Masonry className={'AlbumList'} style={style} onClick={handleClick}>
      {childElements}
    </Masonry>
  );
};

export default AlbumList;
