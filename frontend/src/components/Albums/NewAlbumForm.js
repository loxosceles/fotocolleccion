import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import ClearIcon from '@material-ui/icons/Clear';
import GenericModal from '../ui/GenericModal';

import useStyles from '../../styles/newAlbumsFormStyles';

const NewAlbumForm = () => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);

  const handleModalClose = () => {
    setIsOpen(false);
  };

  const handleModalOpen = () => {
    setIsOpen(true);
  };

  return (
    <div className="container">
      <form>
        <Button
          onClick={handleModalOpen}
          // variant="cancel"
          className={classes.fab}
          color="secondary"
        >
          {!isOpen && <AddIcon />}
        </Button>
        {isOpen && (
          <GenericModal
            isOpen={isOpen}
            handleClose={handleModalClose}
            title="Add Album"
          >
            <h1>Add a new Album</h1>
          </GenericModal>
        )}
      </form>
    </div>
  );
};

export default NewAlbumForm;
