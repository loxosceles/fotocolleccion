import React, { useContext } from 'react';
import { useQuery } from '@apollo/client';
import { UserContext } from '../contexts/UserContext';

import Loading from '../components/Shared/Loading';
import Error from '../components/Shared/Error';
import AlbumList from '../components/Albums/AlbumList';
import NewAlbumForm from './../components/Albums/NewAlbumForm';

import { GET_ALBUMS } from '../apollo/getAlbums';

import useStyles from '../styles/collectionsStyles';

const Collections = () => {
  const classes = useStyles();

  const { called, loading, error, data } = useQuery(GET_ALBUMS);

  if (called && loading) return <Loading />;
  if (error) return <Error error={error} />;

  return (
    <div className={classes.container}>
      <h1>Collections</h1>
      {data.albums && <AlbumList albums={data.albums} />}
      <NewAlbumForm />
    </div>
  );
};

export default Collections;
