import React, { useContext } from 'react';
import { useQuery } from '@apollo/client';
import Error from '../components/Shared/Error';
import { UserContext } from '../contexts/UserContext';

import Loading from '../components/Shared/Loading';
import {
  Card,
  CardHeader,
  Avatar,
  // Paper,
  // Typography,
  // Divider,
} from '@material-ui/core';

import { GET_PROFILE } from '../apollo/getProfile';
import useStyles from '../styles/profileStyles';

const Profile = () => {
  const { user } = useContext(UserContext);
  const id = user.id;
  console.log('user id: ', id);
  const { loading, error, data } = useQuery(GET_PROFILE, {
    variables: { id },
  });
  const classes = useStyles();

  if (loading) return <Loading />;
  if (error) return <Error error={error} />;
  console.log(JSON.stringify(data));

  return (
    <div>
      <Card className={classes.card}>
        <CardHeader
          avatar={<Avatar>{data.user.username[0]}</Avatar>}
          title={data.user.username}
          subheader={data.user.email}
        />
      </Card>
    </div>
  );
};

export default Profile;
