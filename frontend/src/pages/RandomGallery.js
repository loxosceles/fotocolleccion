import React from 'react';
import { Link } from 'react-router-dom';

function RandomGallery() {
  return (
    <div>
      <h1>Random Gallery</h1>
      <Link to="/collections">User's Collections</Link>
    </div>
  );
}

export default RandomGallery;
