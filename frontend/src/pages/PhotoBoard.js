import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/client';
import { Link } from 'react-router-dom';
import cloneDeep from 'lodash/cloneDeep';

import Error from '../components/Shared/Error';
import Loading from '../components/Shared/Loading';
import UploadPhotoForm from '../components/Upload/UploadPhotoForm';
import Gallery from 'react-grid-gallery';

import { GET_ALBUM_PHOTOS } from '../apollo/getAlbumPhotos';

const ThumbComponent = ({ imageProps }) => {
  // console.log('image props', imageProps);
  return <img {...imageProps} />;
};

function PhotoBoard({ collection }) {
  const { called, loading, error, data } = useQuery(GET_ALBUM_PHOTOS, {
    variables: { albumId: collection.id },
  });

  if (called && loading) return <Loading />;
  if (error) return <Error error={error} />;

  return (
    <div style={{ width: '101%' }}>
      {!data.photos || (!!data.photos && data.photos.length === 0) ? (
        <h2> Nothing here. Upload some photos!</h2>
      ) : (
        <Gallery
          images={cloneDeep(data.photos)}
          thumbnailImageComponent={ThumbComponent}
        />
      )}
      <div>
        <Link to="/collections">Back to Collections</Link>
        <UploadPhotoForm
          albumId={collection.id}
          albumTitle={collection.title}
        />
      </div>
    </div>
  );
}

export default PhotoBoard;
