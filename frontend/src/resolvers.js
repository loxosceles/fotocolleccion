import { gql } from '@apollo/client';

export const typeDefs = gql`
  extend type Query {
    isLoggedIn: Boolean!
  }
`;

// export const resolvers = {
//   Mutation: {
//     addOrRemoveFromCart: (_, { id }, { cache }) => {
//       const queryResult = cache.readQuery({
//         query: GET_CART_ITEMS,
//       });
//
//       if (queryResult) {
//         const { cartItems } = queryResult;
//         const data = {
//           cartItems: cartItems.includes(id)
//             ? cartItems.filter((i) => i !== id)
//             : [...cartItems, id],
//         };
//
//         cache.writeQuery({ query: GET_CART_ITEMS, data });
//         return data.cartItems;
//       }
//       return [];
//     },
//   },
// };
