import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: 0,
    padding: 0,
  },
  grow: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    textDecoration: 'none',
  },
  logo: {
    marginRight: theme.spacing(1),
    fontSize: 45,
  },
  faceIcon: {
    marginRight: theme.spacing(1),
    fontSize: 30,
    color: 'white',
  },
  username: {
    color: 'white',
    fontSize: 30,
  },
}));

