import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  snackbar: {
    margin: theme.spacing(1),
  },
}));

