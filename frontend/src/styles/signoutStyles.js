import { makeStyles } from '@material-ui/core/styles';

export default makeStyles({
  root: {
    cursor: 'pointer',
    display: 'flex',
  },
  buttonIcon: {
    marginLeft: '5px',
  },
});
