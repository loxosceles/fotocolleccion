import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  root: {
    width: '100vw',
    textAlign: 'center',
  },
  progress: {
    margin: theme.spacing(2),
    color: theme.palette.secondary.dark,
  },
}));

